package cg.createevent.Pages;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import cg.createevent.EventHelperSingleton;
import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.Helpers.VerticalSpace;
import cg.createevent.R;
import cg.createevent.Adapter.EventAdapter;

/**
 * Created by caner on 10-Apr-16.
 */
public class NewEventFragment extends Fragment {
    RecyclerView myRecycleview;
    EventAdapter adapter;
    ArrayList<EventModel> getInfos;
    EventModel model;
    ProgressBar bar;
    private final String LOG_TAG = NewEventFragment.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.newevent_listview, container, false);

        Firebase.setAndroidContext(getActivity());
        myRecycleview = (RecyclerView) rootView.findViewById(R.id.recycleview);
        bar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        getPosts();

        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getPosts();
            }
        }, new IntentFilter("eventCreated"));

        return rootView;
    }


    private void getPosts() {
        final ArrayList<EventModel> infos = new ArrayList<>();
        Firebase ref = new Firebase(Config.FIREBASE_URL);
        bar.setVisibility(View.VISIBLE);

        Firebase postRef = ref.child("events");
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    EventModel model = new EventModel();
                    GetEvent post = userSnapshot.getValue(GetEvent.class);
                    model.setEventTopic(post.getTopic());
                    model.setEventInformation(post.getInformation());
                    model.setEventLocation(post.getLocation());
                    model.setEventDate(post.getDate());
                    model.setEventTime(post.getTime());
                    model.setEventInterest1(post.getFirstinterest());
                    model.setEventInterest2(post.getSecondinterest());
                    model.setEventInterest3(post.getThirdinterest());

                    if(post.getFirstimage() == null){
                        model.setBitmap1(null);
                    }else {
                        model.setBitmap1(decodeBase64(post.getFirstimage()));
                    }

                    model.setCreatedTime(post.getCreatedtime());
                    model.setUsername(post.getUsername());

                    infos.add(model);
                }

                adapter = new EventAdapter(getContext(),infos);
                myRecycleview.addItemDecoration(new VerticalSpace(30));
                myRecycleview.setLayoutManager(new LinearLayoutManager(getContext()));
                myRecycleview.setAdapter(adapter);
                bar.setVisibility(View.GONE);

                EventHelperSingleton.getInstance().setInfosArray(infos);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                adapter = new EventAdapter(getContext(), infos);
               // myRecycleview.addItemDecoration(new VerticalSpace(10));
                myRecycleview.setLayoutManager(new LinearLayoutManager(getContext()));
                myRecycleview.setAdapter(adapter);
                bar.setVisibility(View.GONE);
            }
        });
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


}
