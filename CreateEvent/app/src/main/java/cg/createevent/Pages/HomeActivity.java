package cg.createevent.Pages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cg.createevent.Adapter.JoinedEventAdapter;
import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetJoinedEvents;
import cg.createevent.Helpers.UserModel;
import cg.createevent.MyPreferences;
import cg.createevent.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView homelistview;
    ArrayList<EventModel> getEvents;
    JoinedEventAdapter adapter;
    CircleImageView image;
    ProgressBar progressBar;
    private static final int RESULT_FIRST_IMAGE = 2;
    Uri firstURI;
    ImageView blankimage;
    TextView text;
    LinearLayout linear;
    private final String LOG_TAG = HomeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        linear = (LinearLayout) findViewById(R.id.homelinearlayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar4);
        blankimage = (ImageView) findViewById(R.id.blank_image);
        text = (TextView) findViewById(R.id.blank_text);



        homelistview = (ListView) findViewById(R.id.homelist);
        getEvents = new ArrayList<EventModel>();
        image = (CircleImageView) findViewById(R.id.circleimage);
        image.setOnClickListener(this);

        adapter = new JoinedEventAdapter(HomeActivity.this, R.layout.home_custom, getEvents);
        homelistview.setAdapter(adapter);

        TextView homeUsername = (TextView) findViewById(R.id.homeid);
        homeUsername.setText(MyPreferences.getUserName(HomeActivity.this));


        homelistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want delete this?");

                // Setting Icon to Dialog
             //   alertDialog.setIcon(R.drawable.delete);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {

                        // Write your code here to invoke YES event
                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();

            }
        });





        addEvents();
        putImage();
    }

    private void putImage() {
        Firebase ref = new Firebase(Config.FIREBASE_URL);
        final Firebase postRef = ref.child("users");
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    UserModel post = userSnapshot.getValue(UserModel.class);
                    try {
                        if (post.getUsername().equals(MyPreferences.getUserName(HomeActivity.this))) {
                            if(post.getImage() != "") {
                                image.setImageURI(Uri.parse(post.getImage()));
                                MyPreferences.setProfilePic(getApplicationContext(),post.getImage());
                                MyPreferences.setProfileName(getApplicationContext(),post.getUsername());
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.notifyDataSetChanged();
    }

    private void addEvents() {

        Firebase ref = new Firebase(Config.FIREBASE_URL);
        final Firebase postRef = ref.child("joinedevents");
        progressBar.setVisibility(View.VISIBLE);

        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    GetJoinedEvents post = userSnapshot.getValue(GetJoinedEvents.class);
                    EventModel model = new EventModel();
                    try {
                        if (post.getUser().equals(MyPreferences.getUserName(HomeActivity.this))) {
                            progressBar.setVisibility(View.GONE);
                            model.setEventTopic(post.getEventname());
                            model.setEventDate(post.getDate());
                            getEvents.add(model);
                            adapter.notifyDataSetChanged();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(progressBar.isShown()){
                    progressBar.setVisibility(View.GONE);
                    linear.setVisibility(View.GONE);
                    blankimage.setVisibility(View.VISIBLE);
                    text.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.circleimage:
                Intent galleryfirst = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryfirst, RESULT_FIRST_IMAGE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_FIRST_IMAGE:
                if (resultCode == RESULT_OK && data != null) {
                    firstURI = data.getData();
                    Glide.with(getApplicationContext()).load(firstURI).into(image);
                    updatePic();
                    break;
                }
        }

    }

    private void updatePic() {
        Firebase ref = new Firebase(Config.FIREBASE_URL);
        final Firebase postRef = ref.child("users");
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    UserModel post = userSnapshot.getValue(UserModel.class);
                    try {
                        if (post.getUsername().equals(MyPreferences.getUserName(HomeActivity.this))) {
                           Firebase var = postRef.child(userSnapshot.getKey());
                            Map<String, Object> picture = new HashMap<String, Object>();
                            picture.put("image",String.valueOf(firstURI));
                            var.updateChildren(picture);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

}