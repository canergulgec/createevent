package cg.createevent.Pages;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cg.createevent.Adapter.EventAdapter;
import cg.createevent.EventHelperSingleton;
import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.Helpers.GetJoinedEvents;
import cg.createevent.MyPreferences;
import cg.createevent.R;

/**
 * Created by caner on 18-Apr-16.
 */
public class DetailedEventActivity extends AppCompatActivity {
    TextView eventTopic, howManyDay, detailTopic;
    ProgressBar detailBar;
    CoordinatorLayout coordinatorLayout;
    private final String LOG_TAG = DetailedEventActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_event);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        detailBar = (ProgressBar) findViewById(R.id.detailBar);

        Bundle intent = getIntent().getExtras();
        int position = intent.getInt("position");
        EventModel model = EventHelperSingleton.getInstance().getInfosArray().get(position);

        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        if (model.getBitmap1() == null) {
            imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.activity));
        } else {
            imageView.setImageBitmap(model.getBitmap1());
        }

        eventTopic = (TextView) findViewById(R.id.eventTopic);
        eventTopic.setText(model.getEventTopic());

        StringBuilder month = getMonth(model.getEventDate());
        TextView detailDate = (TextView) findViewById(R.id.detailDate);
        detailDate.setText(month);


        String PmOrAm = getPmOrAm(model.getEventTime());
        TextView detailTime = (TextView) findViewById(R.id.detailTime);
        detailTime.setText(" " + model.getEventTime());
        detailTime.append(PmOrAm + " ");

        detailTopic = (TextView) findViewById(R.id.detailTopic);
        detailTopic.setText(model.getEventTopic());

        TextView detailUsername = (TextView) findViewById(R.id.detailusername);
        detailUsername.setText(model.getUsername());


        TextView detailedInterest = (TextView) findViewById(R.id.detailinterest1);
        if (model.getEventInterest2() != null) {
            detailedInterest.setText("#");
            detailedInterest.append(model.getEventInterest1());
        }

        TextView eventDetailedInterest2 = (TextView) findViewById(R.id.detailinterest2);
        if (model.getEventInterest2() != null) {
            eventDetailedInterest2.setText("#");
            eventDetailedInterest2.append(model.getEventInterest2());
        }

        TextView eventDetailedInterest3 = (TextView) findViewById(R.id.detailinterest3);
        if (model.getEventInterest3() != null) {
            eventDetailedInterest3.setText("#");
            eventDetailedInterest3.append(model.getEventInterest3());
        }

        TextView detailDay = (TextView) findViewById(R.id.detailDay);
        String date = model.getEventDate();
        String returnDay = getDay(date);
        detailDay.setText(returnDay + " at" + " " + model.getEventTime() + " " + PmOrAm);


        howManyDay = (TextView) findViewById(R.id.dayleft);
        String modelDate = model.getEventDate();
        String currentDate = getCurrentDate();

        long diff = getDifferentiateDates(modelDate, currentDate);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weekInMilli = daysInMilli * 7;

        long elapsedDays = diff / daysInMilli;
        long elapsedWeeks = diff / weekInMilli;

        if (elapsedWeeks == 0) {
            howManyDay.setText(" " + elapsedDays + "d" + "left" + " ");
        } else {
            howManyDay.setText(" " + elapsedWeeks + "w " +
                    (elapsedDays - (elapsedWeeks * 7)) + "d " + "left" + " ");
        }

        TextView detailLocation = (TextView) findViewById(R.id.detailLocation);
        detailLocation.setText(model.getEventLocation());


        TextView detailInfo = (TextView) findViewById(R.id.detailInfo);
        detailInfo.setText(model.getEventInformation());

        Button joinButton = (Button) findViewById(R.id.joinButton);
        if (MyPreferences.getUserName(getApplicationContext()).equals(detailUsername.getText().toString())) {
            joinButton.setEnabled(false);
            joinButton.setBackgroundColor(Color.GRAY);
        }

    }


    private StringBuilder getMonth(String eventDate) {
        StringBuilder stringBuilder = new StringBuilder();

        String month = eventDate.substring(3, 5);
        if (Integer.parseInt(month) == 1) {
            month = " January, ";
        } else if (Integer.parseInt(month) == 2) {
            month = " February, ";
        } else if (Integer.parseInt(month) == 3) {
            month = " March, ";
        } else if (Integer.parseInt(month) == 4) {
            month = " April";
        } else if (Integer.parseInt(month) == 5) {
            month = " May, ";
        } else if (Integer.parseInt(month) == 6) {
            month = " June, ";
        } else if (Integer.parseInt(month) == 7) {
            month = " July, ";
        } else if (Integer.parseInt(month) == 8) {
            month = " August, ";
        } else if (Integer.parseInt(month) == 9) {
            month = " September, ";
        } else if (Integer.parseInt(month) == 10) {
            month = " October, ";
        } else if (Integer.parseInt(month) == 11) {
            month = " November, ";
        } else if (Integer.parseInt(month) == 12) {
            month = " December, ";
        }

        String day = eventDate.substring(0, 2);
        String year = eventDate.substring(6);

        stringBuilder.append(day);
        stringBuilder.append(month);
        stringBuilder.append(year);

        return stringBuilder;
    }

    private String getPmOrAm(String eventTime) {
        String time = eventTime.substring(0, 2);
        if (Integer.parseInt(time) < 12) {
            time = " AM";
        } else {
            time = " PM";
        }
        return time;
    }

    private String getDay(String date) {
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat format2 = new SimpleDateFormat("EEEE");
        String finalDay = format2.format(dt1);

        return finalDay;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        String time = sdf.format(c.getTime());
        return time;
    }

    private long getDifferentiateDates(String modelDate, String currentDate) {
        java.text.DateFormat df = new java.text.SimpleDateFormat("dd/M/yyyy");
        java.util.Date date1 = null;
        try {
            date1 = df.parse(modelDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.util.Date date2 = null;
        try {
            date2 = df.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = date1.getTime() - date2.getTime();
        return diff;
    }


    public void joinClicked(View view) {
        detailBar.setVisibility(View.VISIBLE);
        Firebase ref = new Firebase(Config.FIREBASE_URL);

        final Firebase postRef = ref.child("events");
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    GetEvent post = userSnapshot.getValue(GetEvent.class);
                    if (post.getTopic().equalsIgnoreCase(eventTopic.getText().toString())) {

                        // Firebase updateRef = new Firebase(postRef + "/" + userSnapshot.getKey());
                        //   updateRef.child("joinnumber").setValue(String.valueOf(Integer.parseInt(numberJoin) + 1));
                        addEventToUser();

                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

    }


    private void addEventToUser() {
        final Firebase ref = new Firebase(Config.FIREBASE_URL);

        final Firebase kk = ref.child("joinedevents");
        kk.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    GetJoinedEvents post = userSnapshot.getValue(GetJoinedEvents.class);
                    if(post.getUser().equals(MyPreferences.getUserName(getApplicationContext())) && detailBar.isShown()){
                        if(post.getEventname().equals(detailTopic.getText().toString())){
                            detailBar.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Already registered to event", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    }
                }

                if(detailBar.isShown()) {
                    final Firebase postRef = ref.child("joinedevents");
                    Map<String, String> post = new HashMap<String, String>();
                    post.put("user", MyPreferences.getUserName(getApplicationContext()));
                    post.put("eventname", eventTopic.getText().toString());
                    post.put("date", howManyDay.getText().toString());
                    postRef.push().setValue(post);

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Successfully joined to event", Snackbar.LENGTH_LONG);

                    snackbar.show();
                    detailBar.setVisibility(View.GONE);

                }

            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

}

