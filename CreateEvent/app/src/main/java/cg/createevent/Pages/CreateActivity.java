package cg.createevent.Pages;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;


import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.MyPreferences;
import cg.createevent.R;

/**
 * Created by caner on 11-Apr-16.
 */
public class CreateActivity extends AppCompatActivity implements View.OnClickListener {
    private final String LOG_TAG = CreateActivity.class.getSimpleName();

    private EditText inputTopic, inputInformation;
    private EditText inputLocation;
    private TextInputLayout inputLayoutTopic;
    private TextInputLayout inputLayoutInformation, inputLayoutLocation, inputLayoutAttendance;
    private Spinner spinner, spinner2, spinner3;
    private DatePicker datePicker;
    private String dateValue;
    private TimePicker timePicker;
    private String timeValue;
    private ImageView firstImage;
    private Button createButton;
    private String firstSpinner;
    private String secondSpinner;
    private String thirdSpinner;

    private String firstBase64;

    private static final int RESULT_FIRST_IMAGE = 1;

    String clickedTime;
    private String firstImagepath=null;

    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create);
        Firebase.setAndroidContext(this);

        username = MyPreferences.getUserName(getApplicationContext());

        inputTopic = (EditText) findViewById(R.id.etTopic);
        inputInformation = (EditText) findViewById(R.id.etInformation);
        inputLocation = (EditText) findViewById(R.id.etLocation);

        inputLayoutTopic = (TextInputLayout) findViewById(R.id.input_layout_topic);
        inputLayoutInformation = (TextInputLayout) findViewById(R.id.input_layout_information);
        inputLayoutLocation = (TextInputLayout) findViewById(R.id.input_layout_location);

        inputTopic.addTextChangedListener(new MyTextWatcher(inputTopic));
        inputInformation.addTextChangedListener(new MyTextWatcher(inputInformation));
        inputLocation.addTextChangedListener(new MyTextWatcher(inputLocation));

        firstImage = (ImageView) findViewById(R.id.first);
        firstImage.setOnClickListener(this);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner3 = (Spinner) findViewById(R.id.spinner3);

        datePicker = (DatePicker) findViewById(R.id.datePicker);
        getSelectedDate();


        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        getSelectedTime();

        createButton = (Button) findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedTime = getButtonClickedTime();
                checkValidates();
            }
        });


        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Choose Interest");
        categories.add("Sport");
        categories.add("Business");
        categories.add("Education");
        categories.add("Party");
        categories.add("Video Games");
        categories.add("Shopping");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);
        spinner2.setAdapter(dataAdapter);
        spinner3.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                firstSpinner = parent.getItemAtPosition(position).toString();
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER);

                if (!(firstSpinner.equals("Choose Interest"))) {
                    spinner2.setVisibility(View.VISIBLE);
                    makeVisibleSpinner2();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private String getButtonClickedTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
        String time = sdf.format(c.getTime());

        return time;
    }

    private void checkValidates() {
        if (!validateTopic()) {
            return;
        }
        if (!validateInformation()) {
            return;
        }
        if (!validateLocation()) {
            return;
        }

        addAttributesToDB();

    }

    private void getSelectedDate() {
        datePicker.init(
                datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        int selectedYear = year;
                        int selectedMonth = monthOfYear + 1;
                        int selectedDayOfMonth = dayOfMonth;

                        String strMonth = "" + selectedMonth;
                        String strDayofMonth = "" + selectedDayOfMonth;

                        if(selectedMonth < 10){
                            strMonth = "0" + selectedMonth;
                        }
                        if(selectedDayOfMonth < 10){
                            strDayofMonth = "0" + selectedDayOfMonth;
                        }
                        dateValue = "" + strDayofMonth + "/" + strMonth + "/" + selectedYear;
                    }

                });
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String date = sdf.format(c.getTime());

        return date;
    }


    private void getSelectedTime() {
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String strHour = "" + hourOfDay;
                String strMinute = "" + minute;

                if(hourOfDay < 10){
                    strHour = "0" + hourOfDay;
                }
                if(minute < 10){
                    strMinute = "0" + minute;
                }
                timeValue = strHour + ":" + strMinute;
            }
        });
    }

    private String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String time = sdf.format(c.getTime());

        return time;
    }


    private void makeVisibleSpinner2() {
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                secondSpinner = parent.getItemAtPosition(position).toString();
                if (!(secondSpinner.equals("Choose Interest"))) {
                    spinner3.setVisibility(View.VISIBLE);
                    makeVisibleSpinner3();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void makeVisibleSpinner3() {
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                thirdSpinner = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.first:
                Intent galleryfirst = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryfirst,RESULT_FIRST_IMAGE);
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case RESULT_FIRST_IMAGE:
                if(resultCode == RESULT_OK && data != null) {
                    Uri firstURI = data.getData();
                    Glide.with(getApplicationContext()).load(firstURI).into(firstImage);
                    firstImagepath = getPath(firstURI);
                    try{
                        Bitmap bitmap= decodeUri(getApplicationContext(), firstURI, 300); //BitmapFactory.decodeFile(firstImagepath);
                        firstBase64 = convertToBase64(bitmap);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    break;
                }
          }
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }


    private String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String convertToBase64(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        return encodedImage;
    }


    private boolean validateTopic() {
        if (inputTopic.getText().toString().trim().isEmpty()) {
            inputLayoutTopic.setError(getString(R.string.err_msg_topic));
            requestFocus(inputTopic);
            return false;
        } else {
            inputLayoutTopic.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateInformation() {
        if (inputInformation.getText().toString().trim().isEmpty()) {
            inputLayoutInformation.setError(getString(R.string.err_msg_info));
            requestFocus(inputInformation);
            return false;
        } else {
            inputLayoutInformation.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLocation() {
        if (inputLocation.getText().toString().trim().isEmpty()) {
            inputLayoutLocation.setError(getString(R.string.err_msg_location));
            requestFocus(inputLocation);
            return false;
        } else {
            inputLayoutLocation.setErrorEnabled(false);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etTopic:
                    validateTopic();
                    break;
                case R.id.etInformation:
                    validateInformation();
                    break;
                case R.id.etLocation:
                    validateLocation();
                    break;
            }
        }
    }

    public void addAttributesToDB(){
        EventModel model = new EventModel();
        model.setUsername(username);
        model.setEventTopic(inputTopic.getText().toString());
        model.setEventInformation(inputInformation.getText().toString());
        model.setEventLocation(inputLocation.getText().toString());

        if(dateValue != null) {
            model.setEventDate(dateValue);
        }else{
            String currentDate = getCurrentDate();
            model.setEventDate(currentDate);
        }

        if(timeValue != null) {
            model.setEventTime(timeValue);
        }else{
            String currentTime = getCurrentTime();
            model.setEventTime(currentTime);
        }

        if(!(firstSpinner.equals("Choose Interest"))){
            model.setEventInterest1(firstSpinner);
        }
        if((secondSpinner != null)){
            if (secondSpinner != "Choose Interest") {
                model.setEventInterest2(secondSpinner);
            }
        }
        if((thirdSpinner != null)) {
            if (thirdSpinner != "Choose Interest") {
                model.setEventInterest3(thirdSpinner);
            }
        }

        model.setEventImage1(firstBase64);
        model.setCreatedTime(clickedTime);
        model.setCreatedTime(clickedTime);

        //Creating firebase object
        Firebase ref = new Firebase(Config.FIREBASE_URL);

        Firebase postRef = ref.child("events");
        Map<String, String> post = new HashMap<String, String>();
        post.put("topic", model.getEventTopic());
        post.put("information", model.getEventInformation());
        post.put("location", model.getEventLocation());
        post.put("date", model.getEventDate());
        post.put("time", model.getEventTime());
        post.put("firstinterest", model.getEventInterest1());
        post.put("secondinterest", model.getEventInterest2());
        post.put("thirdinterest", model.getEventInterest3());
        post.put("firstimage", model.getEventImage1());
        post.put("createdtime", model.getCreatedTime());
        if(model.getUsername() != null) {
            post.put("username", model.getUsername());
        }else{
            post.put("username", "canerg");  //
        }

        postRef.push().setValue(post);

        sendBroadcast(new Intent("eventCreated"));
        finish();
    }

}

