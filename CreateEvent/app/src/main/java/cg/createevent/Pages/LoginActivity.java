package cg.createevent.Pages;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.support.v7.widget.AppCompatPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.Helpers.UserModel;
import cg.createevent.MyPreferences;
import cg.createevent.R;


public class LoginActivity extends AppCompatActivity {
    public EditText inputUsername, inputPassword;
    private TextInputLayout inputLayoutUsername, inputLayoutPassword;
    private Button btnLogin;
    private ProgressBar loginbar;
    int count = 0;
    private static final String TAG = "sd";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Firebase.setAndroidContext(this);

        inputUsername = (EditText) findViewById(R.id.input_username);
        inputPassword = (EditText) findViewById(R.id.input_password);

        inputLayoutUsername = (TextInputLayout) findViewById(R.id.textInputUsername);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputPassword);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginbar = (ProgressBar) findViewById(R.id.loginbar);

        if(MyPreferences.hasUserLoggedIn(getApplicationContext())){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }


        inputUsername.addTextChangedListener(new MyTextWatcher(inputUsername));
        inputPassword.addTextChangedListener(new MyTextWatcher(inputPassword));

        Log.v(TAG,"tjg " + inputPassword.getText().toString());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateUsername() && !validatePassword()) {
                    return;

                }else{
                    checkUserAndPass();

                }
            }
        });

        TextView signUp = (TextView) findViewById(R.id.link_signup);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));

            }
        });

    }

    private void checkUserAndPass() {
        loginbar.setVisibility(View.VISIBLE);
        Firebase ref = new Firebase(Config.FIREBASE_URL);
        Firebase postRef = ref.child("users");

        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    UserModel post = userSnapshot.getValue(UserModel.class);
                    if (inputUsername.getText().toString().equals(post.getUsername())) {
                        if (inputPassword.getText().toString().equals(post.getPassword())) {
                            loginbar.setVisibility(View.GONE);
                            MyPreferences.setUserLoggedIn(getApplicationContext(),true);
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);

                            MyPreferences.setUserName(getApplicationContext(),inputUsername.getText().toString());
                            startActivity(intent);
                            finish();
                        }
                    }
                }
                if(loginbar.isShown()){
                    loginbar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this,"Check your information" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private boolean validateUsername() {
        if (inputUsername.getText().toString().trim().isEmpty()) {
            inputLayoutUsername.setError(getString(R.string.err_msg_username));
            requestFocus(inputUsername);
            return false;
        } else {
            inputLayoutUsername.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_username:
                    validateUsername();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }
}