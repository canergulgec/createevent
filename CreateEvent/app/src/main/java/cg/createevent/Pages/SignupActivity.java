package cg.createevent.Pages;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.collection.LLRBEmptyNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.Helpers.UserModel;
import cg.createevent.MyPreferences;
import cg.createevent.R;


public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    Button signUp;
    EditText name, username, password;
    ProgressBar bar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Firebase.setAndroidContext(this);

        TextView loginLink = (TextView) findViewById(R.id.link_login);
        signUp = (Button) findViewById(R.id.btn_signup);
        name = (EditText) findViewById(R.id.signup_name);
        username = (EditText) findViewById(R.id.signup_username);
        password = (EditText) findViewById(R.id.signup_password);
        bar = (ProgressBar) findViewById(R.id.progressBar2);


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar.setVisibility(View.VISIBLE);
                Firebase ref = new Firebase(Config.FIREBASE_URL);
                final Firebase postRef = ref.child("users");

                postRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            UserModel post = userSnapshot.getValue(UserModel.class);
                            if(post.getUsername().equals(username.getText().toString())){
                               bar.setVisibility(View.GONE);
                            }
                        }
                        if(bar.isShown()){
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("name", name.getText().toString());
                            map.put("username", username.getText().toString());
                            map.put("password", password.getText().toString());
                            map.put("image", "");
                            postRef.push().setValue(map);

                            Toast.makeText(SignupActivity.this, "Succesfully Signed up !", Toast.LENGTH_SHORT).show();

                            finish();
                        }else{
                            Toast.makeText(SignupActivity.this, "User is already exists!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
            }

        });


        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });

    }
}
