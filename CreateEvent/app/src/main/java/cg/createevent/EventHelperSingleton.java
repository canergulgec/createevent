package cg.createevent;

import java.util.ArrayList;

import cg.createevent.Helpers.EventModel;

public class EventHelperSingleton {

    private ArrayList<EventModel> infos = new ArrayList<EventModel>();

    private static EventHelperSingleton ourInstance;
    public static EventHelperSingleton getInstance() {
        if(ourInstance == null){
            ourInstance = new EventHelperSingleton();
        }
        return ourInstance;
    }

    private EventHelperSingleton() {
    }

    public void setInfosArray(ArrayList<EventModel> infos){
        this.infos = infos;
    }

    public ArrayList<EventModel> getInfosArray(){
        return infos;
    }
}
