package cg.createevent.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cg.createevent.Animation.AnimationUtil;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Pages.DetailedEventActivity;
import cg.createevent.R;

/**
 * Created by caner on 17-Apr-16.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {
    ArrayList<EventModel> getItems;
    Context context;
    private int previousPosition = 0;


    public EventAdapter(Context context, ArrayList<EventModel>getItems){
        this.context = context;
        this.getItems = getItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.newevent,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        EventModel current = getItems.get(position);
        holder.myEvent_topic.setText(current.getEventTopic());


        StringBuilder month = getMonth(current.getEventDate());
        holder.myEvent_date.setText(month);

        String PmOrAm = getPmOrAm(current.getEventTime());
        holder.myEvent_time.setText(current.getEventTime());
        holder.myEvent_time.append(PmOrAm);



        if(current.getBitmap1() ==null){
            holder.myEvent_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.activity));
        }else{
            holder.myEvent_image.setImageBitmap(current.getBitmap1());
        }

        String currentime = getCurrentTime();
        java.text.DateFormat df = new java.text.SimpleDateFormat("dd/M/yyyy HH:mm");
        java.util.Date date1 = null;
        try {
            date1 = df.parse(currentime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.util.Date date2 = null;
        try {
            date2 = df.parse(current.getCreatedTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = date1.getTime() - date2.getTime();
        long diffForweek = date1.getTime() - date2.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weekInMilli = daysInMilli * 7;

        long elapsedDays = diff / daysInMilli;
        diff = diff % daysInMilli;

        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long elapsedWeeks = diffForweek / weekInMilli;


        if(elapsedMinutes < 60 && elapsedHours == 0 && elapsedDays == 0){
            holder.myEvent_created_time.setText("" + elapsedMinutes + "m " + "ago");
        }else if(elapsedHours > 0 && elapsedDays == 0){
            holder.myEvent_created_time.setText("" + elapsedHours + "h " + "ago");
        }else if(elapsedDays > 0 && elapsedDays < 7){
            holder.myEvent_created_time.setText("" + elapsedDays + "d " + "ago");
        }else{
            holder.myEvent_created_time.setText("" + elapsedWeeks + "w " + "ago");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailedEventActivity.class);
                intent.putExtra("position", position);

                context.startActivity(intent);
            }
        });

        if(position > previousPosition){ // We are scrolling DOWN

            AnimationUtil.animate(holder, true);

        }else{ // We are scrolling UP

            AnimationUtil.animate(holder, false);

        }

        previousPosition = position;

    }


    private String getPmOrAm(String eventTime) {
        String time = eventTime.substring(0,2);
        if(Integer.parseInt(time) < 12){
            time = " AM";
        }else{
            time = " PM";
        }
        return time;
    }

    private StringBuilder getMonth(String eventDate) {
        StringBuilder stringBuilder = new StringBuilder();

        String month = eventDate.substring(3,5);
        if(Integer.parseInt(month) == 1){
            month = " January, ";
        }else if(Integer.parseInt(month) == 2){
            month = " February, ";
        }else if(Integer.parseInt(month) == 3){
            month = " March, ";
        }else if(Integer.parseInt(month) == 4){
            month = " April";
        }else if(Integer.parseInt(month) == 5){
            month = " May, ";
        }else if(Integer.parseInt(month) == 6){
            month = " June, ";
        }else if(Integer.parseInt(month) == 7){
            month = " July, ";
        }else if(Integer.parseInt(month) == 8){
            month = " August, ";
        }else if(Integer.parseInt(month) == 9){
            month = " September, ";
        }else if(Integer.parseInt(month) == 10){
            month = " October, ";
        }else if(Integer.parseInt(month) == 11){
            month = " November, ";
        }else if(Integer.parseInt(month) == 12){
            month = " December, ";
        }

        String day = eventDate.substring(0,2);
        String year = eventDate.substring(6);

        stringBuilder.append(day);
        stringBuilder.append(month);
        stringBuilder.append(year);

        return stringBuilder;

    }

    private String getCurrentTime() {
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
        String currentime = sdf.format(ca.getTime());
        return currentime;
    }


    @Override
    public int getItemCount() {
        return getItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView myEvent_topic,myEvent_date,myEvent_time;
        ImageView  myEvent_image;
        TextView myEvent_created_time;
        public MyViewHolder(View itemView) {
            super(itemView);

            myEvent_topic = (TextView) itemView.findViewById(R.id.tvTopic);
            myEvent_date = (TextView) itemView.findViewById(R.id.tvDate);
            myEvent_time = (TextView) itemView.findViewById(R.id.tvTime);
            myEvent_image = (ImageView) itemView.findViewById(R.id.eventImage);
            myEvent_created_time = (TextView) itemView.findViewById(R.id.tvCreatedTime);
        }
    }
}