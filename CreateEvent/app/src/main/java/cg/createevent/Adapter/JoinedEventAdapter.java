package cg.createevent.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cg.createevent.Helpers.Config;
import cg.createevent.Helpers.EventModel;
import cg.createevent.Helpers.GetEvent;
import cg.createevent.Helpers.GetJoinedEvents;
import cg.createevent.Pages.LoginActivity;
import cg.createevent.R;

/**
 * Created by caner on 28-May-16.
 */
public class JoinedEventAdapter extends ArrayAdapter {

    private List<EventModel> objects;
    private LayoutInflater inflater;
    private int resource;
    TextView isim;
    private final String LOG_TAG = JoinedEventAdapter.class.getSimpleName();

    public JoinedEventAdapter(Context context, int resource, ArrayList<EventModel> objects) {
        super(context, resource, objects);

        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(resource, null);
            holder.myEvent_topic = (TextView) convertView.findViewById(R.id.eventname);
            holder.myEvent_dat = (TextView) convertView.findViewById(R.id.eventdate);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.myEvent_topic.setText(objects.get(position).getEventTopic());
        holder.myEvent_dat.setText(objects.get(position).getEventDate());



            return convertView;

    }

    public class ViewHolder {
        public TextView myEvent_topic;
        public TextView myEvent_dat;

    }

}
