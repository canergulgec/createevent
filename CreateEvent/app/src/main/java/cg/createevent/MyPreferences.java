package cg.createevent;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;


public class MyPreferences {

    private static final String PREFERENCES_FILE = "EventApp";
    private static final String PREFERENCE_USER_LOGIN = "UserLogin";
    private static final String PREFERENCE_USER_NAME = "UserName";
    private static final String PREFERENCE_PROFILE_PIC = "profile";
    private static final String PREFERENCE_PROFILE_NAME = "profName";


    public static void setUserLoggedIn(Context context, boolean flag){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(PREFERENCE_USER_LOGIN, flag);
        editor.apply();
    }

    public static boolean hasUserLoggedIn(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getBoolean(PREFERENCE_USER_LOGIN, false);
    }

    public static void setUserName(Context context, String username){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_USER_NAME, username);
        editor.apply();
    }

    public static String getUserName(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getString(PREFERENCE_USER_NAME, "");
    }

    public static void setProfilePic(Context context, String profile){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_PROFILE_PIC, profile);
        editor.apply();
    }

    public static String getProfilePic(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getString(PREFERENCE_PROFILE_PIC, "");
    }

    public static void setProfileName(Context context, String profileName){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_PROFILE_NAME, profileName);
        editor.apply();
    }

    public static String getProfileName(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getString(PREFERENCE_PROFILE_NAME, "");
    }
}


