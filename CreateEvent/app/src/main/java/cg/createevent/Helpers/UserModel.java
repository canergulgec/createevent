package cg.createevent.Helpers;

/**
 * Created by caner on 21-May-16.
 */
public class UserModel {
    private String name;
    private String username;
    private String password;
    private String image;

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getImage() {
        return image;
    }
}
