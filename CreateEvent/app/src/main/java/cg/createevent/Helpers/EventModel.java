package cg.createevent.Helpers;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by caner on 15-Apr-16.
 */
public class EventModel implements Parcelable {
    public String eventTopic;
    public String eventInformation;
    public String eventLocation;
    public String eventDate;
    public String eventTime;
    public String eventInterest1;
    public String eventInterest2;
    public String eventInterest3;
    public String eventImage1;
    public String eventImage2;
    public String eventImage3;
    public String eventImage4;
    public Bitmap bitmap1;
    public String createdTime;
    public String username;


    public EventModel() {

    }

    protected EventModel(Parcel in) {
        eventTopic = in.readString();
        eventInformation = in.readString();
        eventLocation = in.readString();
        eventDate = in.readString();
        eventTime = in.readString();
        eventInterest1 = in.readString();
        eventInterest2 = in.readString();
        eventInterest3 = in.readString();
        eventImage1 = in.readString();
        eventImage2 = in.readString();
        eventImage3 = in.readString();
        eventImage4 = in.readString();
        bitmap1 = in.readParcelable(Bitmap.class.getClassLoader());
        createdTime = in.readString();
        username = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventTopic);
        dest.writeString(eventInformation);
        dest.writeString(eventLocation);
        dest.writeString(eventDate);
        dest.writeString(eventTime);
        dest.writeString(eventInterest1);
        dest.writeString(eventInterest2);
        dest.writeString(eventInterest3);
        dest.writeString(eventImage1);
        dest.writeString(eventImage2);
        dest.writeString(eventImage3);
        dest.writeString(eventImage4);
        dest.writeParcelable(bitmap1, flags);
        dest.writeString(createdTime);
        dest.writeString(username);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public String getEventTopic() {
        return eventTopic;
    }

    public void setEventTopic(String eventTopic) {
        this.eventTopic = eventTopic;
    }

    public String getEventInformation() {
        return eventInformation;
    }

    public void setEventInformation(String eventInformation) {
        this.eventInformation = eventInformation;
    }

    public void setBitmap1(Bitmap bitmap){
        bitmap1 = bitmap;
    }

    public Bitmap getBitmap1(){
        return bitmap1;
    }

    public String getEventImage1() {
        return eventImage1;
    }

    public void setEventImage1(String eventImage1) {
        this.eventImage1 = eventImage1;
    }

    public String getEventInterest3() {
        return eventInterest3;
    }

    public void setEventInterest3(String eventInterest3) {
        this.eventInterest3 = eventInterest3;
    }

    public String getEventInterest2() {
        return eventInterest2;
    }

    public void setEventInterest2(String eventInterest2) {
        this.eventInterest2 = eventInterest2;
    }

    public String getEventInterest1() {
        return eventInterest1;
    }

    public void setEventInterest1(String eventInterest1) {
        this.eventInterest1 = eventInterest1;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
