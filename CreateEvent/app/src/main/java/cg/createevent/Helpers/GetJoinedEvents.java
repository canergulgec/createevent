package cg.createevent.Helpers;

/**
 * Created by caner on 28-May-16.
 */
public class GetJoinedEvents {
    private String eventname;
    private String user;
    private String date;

    public String getEventname() {
        return eventname;
    }

    public String getUser() {
        return user;
    }

    public String getDate() {
        return date;
    }
}
