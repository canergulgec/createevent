package cg.createevent.Helpers;

/**
 * Created by caner on 19-May-16.
 */
public class GetEvent {

    private String topic;
    private String information;
    private String location;
    private String date;
    private String time;
    private String firstinterest;
    private String secondinterest;
    private String thirdinterest;
    private String firstimage;
    private String secondimage;
    private String thirdimage;
    private String fourthimage;
    private String createdtime;
    private String username;

    public String getTopic() {
        return topic;
    }

    public String getInformation() {
        return information;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getFirstinterest() {
        return firstinterest;
    }

    public String getSecondinterest() {
        return secondinterest;
    }

    public String getThirdinterest() {
        return thirdinterest;
    }

    public String getFirstimage() {
        return firstimage;
    }


    public String getCreatedtime() {
        return createdtime;
       }

    public String getUsername() {
        return username;
    }
}
